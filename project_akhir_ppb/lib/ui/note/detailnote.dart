import 'package:flutter/material.dart';

class DetailNotePage extends StatelessWidget {
  final String note;

  const DetailNotePage({Key? key, required this.note}) : super(key: key);

  String getNoteDescription() {
    String description = '';

    switch (note) {
      case 'HTML':
        description =
            'HTML adalah singkatan dari Hypertext Markup Language. Ini adalah bahasa markah standar yang digunakan untuk membangun dan mengatur struktur halaman web. HTML menggunakan tag-tag yang ditempatkan di sekitar konten untuk memberikan makna dan struktur ke halaman web. Tag-tag ini terdiri dari elemen-elemen seperti judul, paragraf, gambar, tautan, tabel, formulir, dan lain-lain. HTML menyediakan kerangka dasar untuk mengorganisir konten dan menampilkan informasi di web.';
        break;
      case 'CSS':
        description =
            'CSS adalah singkatan dari Cascading Style Sheets. Ini adalah bahasa gaya yang digunakan untuk mengendalikan tampilan dan format dari elemen-elemen yang ditampilkan di halaman web. CSS digunakan untuk memberikan warna, ukuran, jenis huruf, margin, padding, tata letak, dan berbagai efek visual lainnya pada elemen-elemen HTML. Dengan menggunakan CSS, Anda dapat memisahkan presentasi visual dari struktur konten HTML, sehingga memungkinkan perubahan gaya secara konsisten di seluruh situs web.';
        break;
      case 'JavaScript':
        description =
            'JavaScript adalah bahasa pemrograman yang digunakan untuk membuat halaman web interaktif. Dengan JavaScript, Anda dapat menambahkan fungsi dan logika ke dalam halaman web Anda. JavaScript memungkinkan manipulasi elemen HTML, menangani interaksi pengguna, memvalidasi input, mengirim permintaan ke server, dan banyak lagi. Bahasa ini sangat fleksibel dan mendukung paradigma pemrograman berorientasi objek serta pemrograman berbasis peristiwa.';
        break;
      case 'Flutter':
        description =
            'Flutter adalah kerangka kerja pengembangan aplikasi lintas platform yang dikembangkan oleh Google. Dengan Flutter, Anda dapat membuat aplikasi berkinerja tinggi untuk iOS, Android, dan platform lainnya menggunakan satu kode sumber tunggal. Flutter menggunakan bahasa pemrograman Dart yang modern dan mendukung pengembangan antarmuka pengguna yang menarik dengan menggunakan widget. Flutter juga menawarkan set alat dan komponen yang kaya untuk membangun dan merancang antarmuka pengguna yang menarik serta memungkinkan akses ke fitur-fitur perangkat seperti kamera, geolokasi, sensor, dan lainnya.';
        break;
    }

    return description;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(note),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(
          getNoteDescription(),
          style: const TextStyle(fontSize: 16.0),
        ),
      ),
    );
  }
}
