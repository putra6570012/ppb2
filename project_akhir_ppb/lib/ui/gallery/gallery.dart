import 'package:flutter/material.dart';
import 'detailgallery.dart';

class GalleryPage extends StatelessWidget {

  // Daftar nama asset gambar beserta deskripsi
  final List<Map<String, String>> imageAssets = [
    {
      'imageUrl': 'images/html5.PNG',
      'description': 'Ini adalah logo HTML5',
    },
    {
      'imageUrl': 'images/css.PNG',
      'description': 'Ini adalah logo CSS',
    },
    {
      'imageUrl': 'images/javascript.PNG',
      'description': 'Ini adalah logo JavaScript',
    },
    {
      'imageUrl': 'images/flutter.PNG',
      'description': 'Ini adalah logo Flutter',
    },
    // Tambahkan nama asset gambar dan deskripsi lain sesuai kebutuhan
  ];

   GalleryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.builder(
        itemCount: imageAssets.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, // Jumlah kolom dalam grid
          mainAxisSpacing: 16.0, // Spasi vertikal antara item
          crossAxisSpacing: 16.0, // Spasi horizontal antara item
          childAspectRatio: 1, // Rasio aspek lebar-tinggi item
        ),
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailGalleryPage(
                    imageUrl: imageAssets[index]['imageUrl']!,
                    imageDescription: imageAssets[index]['description']!,
                  ),
                ),
              );
            },
            child: Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 2.0,
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  imageAssets[index]['imageUrl']!,
                  fit: BoxFit.cover,
                  width: 300,
                  height: 300,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
