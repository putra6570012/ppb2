import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;

class DetailGalleryPage extends StatelessWidget {
  final String imageUrl;
  final String imageDescription;

  const DetailGalleryPage({
    Key? key,
    required this.imageUrl,
    required this.imageDescription,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final imageName = path.basename(imageUrl);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail Gallery'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imageUrl,
              width: 300,
              height: 300,
            ),
            const SizedBox(height: 16),
            Text(
              'Image : $imageName',
            ),
            const SizedBox(height: 16),
            Text(
              'Description : $imageDescription',
              style: const TextStyle(fontSize: 18),
            ),
          ],
        ),
      ),
    );
  }
}
