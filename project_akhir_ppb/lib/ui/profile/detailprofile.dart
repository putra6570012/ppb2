import 'package:flutter/material.dart';

class DetailProfilePage extends StatefulWidget {
  final int index;
  const DetailProfilePage({Key? key, required this.index}) : super(key: key);

  @override
  State<DetailProfilePage> createState() => _DetailProfilePageState();
}

class _DetailProfilePageState extends State<DetailProfilePage> {
  var data = [
    ['031210004', 'M Putra Mulya Pratama', 'D3 Sistem Informasi', 'Laki-laki', 'Palembang, 09 April 2002'],
    ['031190002', 'Masagus Muhammad Ricky', 'D3 Sistem Informasi', 'Laki-laki', 'Palembang, 04 April 1999'],
    ['031210038', 'Muhammad Islam', 'D3 Sistem Informasi', 'Laki-laki', 'Palembang, 1 Mei 2002'],
    ['031210011', 'Raihan Sulthan Anika', 'D3 Sistem Informasi', 'Laki-laki', 'Palembang, 24 Mei 2002']
  ];

  var images = [
    'images/putra.JPEG',
    'images/ricky.jpeg',
    'images/islam.JPEG',
    'images/raihan.jpeg',
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text('Detail Profile'),
        ),
        body: SizedBox(
          height: 300,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center, // Center the text horizontally
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Center(
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      CircleAvatar(
                        radius: 60,
                        backgroundImage: images[widget.index].isEmpty
                            ? null
                            : AssetImage(images[widget.index]), // Replace with the actual image path
                        child: images[widget.index].isEmpty
                            ? const Icon(
                                Icons.people,
                                size: 60,
                                color: Colors.white,
                              )
                            : null,
                      ),
                      images[widget.index].isEmpty
                          ? const Positioned(
                              bottom: 0,
                              child: Text(
                                'No Image',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.center, // Center the text horizontally
                  child: Text(
                    'NPM: ' + data[widget.index][0],
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center, // Center the text horizontally
                  child: Text(
                    'Nama: ' + data[widget.index][1],
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center, // Center the text horizontally
                  child: Text(
                    'Prodi: ' + data[widget.index][2],
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center, // Center the text horizontally
                  child: Text(
                    'Gender: ' + data[widget.index][3],
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center, // Center the text horizontally
                  child: Text(
                    'TTL: ' + data[widget.index][4],
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
