import 'package:flutter/material.dart';
import 'detailprofile.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var name = [
    'M Putra Mulya Pratama',
    'Masagus Muhammad Ricky',
    'Muhammad Islam',
    'Raihan Sulthan Anika',
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: name.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DetailProfilePage(
                  index: index,
                ),
              ),
            );
          },
          child: Container(
            alignment: Alignment.centerLeft,
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.black12,
                ),
              ),
            ),
            height: 50,
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0), // Add left padding here
              child: Text(
                name[index],
                style: const TextStyle(fontSize: 20),
              ),
            ),
          ),
        );
      },
    );
  }
}
