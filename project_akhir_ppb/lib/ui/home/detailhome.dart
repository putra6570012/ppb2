import 'package:flutter/material.dart';

class DetailHome extends StatelessWidget {
  final String programName;

  const DetailHome({Key? key, required this.programName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String description = '';

    if (programName == 'HTML5') {
      description = 'HTML adalah bahasa markup yang digunakan untuk membuat struktur dan konten halaman web. Ini digunakan bersama dengan CSS dan JavaScript untuk membuat tampilan dan interaksi yang menarik.';
    } else if (programName == 'CSS') {
      description = 'CSS (Cascading Style Sheets) adalah bahasa yang digunakan untuk mengatur tampilan dan format dokumen HTML. Dengan CSS, Anda dapat mengubah warna, ukuran, tata letak, dan berbagai aspek visual lainnya dari elemen-elemen di halaman web.';
    } else if (programName == 'JavaScript') {
      description = 'JavaScript adalah bahasa pemrograman tingkat tinggi yang digunakan untuk membuat interaksi pada halaman web. Dengan JavaScript, Anda dapat menambahkan fungsi, mengubah konten secara dinamis, dan berkomunikasi dengan server untuk membuat aplikasi web yang kaya dan interaktif.';
    } else if (programName == 'Flutter') {
      description = 'Flutter adalah kerangka kerja pengembangan aplikasi seluler open-source yang dikembangkan oleh Google. Menggunakan bahasa pemrograman Dart, Flutter memungkinkan Anda untuk membuat antarmuka pengguna yang kaya dan indah untuk platform Android dan iOS dengan menggunakan kode yang sama.';
    } else {
      description = 'Deskripsi untuk $programName belum tersedia.';
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(programName),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                programName,
                style: const TextStyle(fontSize: 24),
              ),
              const SizedBox(height: 16),
              Text(
                description,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}