import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'ui/home/home.dart';
import 'ui/gallery/gallery.dart';
import 'ui/note/note.dart';
import 'ui/profile/profile.dart';

void main() {
  runApp(const CurvedBottomNavigationBarPage());
}

class CurvedBottomNavigationBarPage extends StatefulWidget {
  const CurvedBottomNavigationBarPage({Key? key}) : super(key: key);

  @override
  _CurvedBottomNavigationBarState createState() => _CurvedBottomNavigationBarState();
}

class _CurvedBottomNavigationBarState extends State<CurvedBottomNavigationBarPage> {

  final navigationTitles = const [
    'Home',
    'Gallery',
    'Notes',
    'Profile'];

  final navigationIcons = const [
    Icons.home,
    Icons.image,
    Icons.note,
    Icons.person,
  ];

  int _selectedIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(navigationTitles[_selectedIndex]),
        ),
        body: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() {
              _selectedIndex = index;
            });
          },
          children: [
            const HomePage(),
            GalleryPage(),
            NotePage(),
            const ProfilePage(),
          ],
        ),
        bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Colors.blue,
          index: _selectedIndex,
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
            });
            _pageController.jumpToPage(index);
          },
          items: navigationIcons.map((icon) => buildNavItem(icon)).toList(),
        ),
      ),
    );
  }

  Widget buildNavItem(IconData icon) {
    return InkWell(
      onTap: () {
        int index = navigationIcons.indexOf(icon);
        _pageController.jumpToPage(index);
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Icon(icon),
      ),
    );
  }
}
